#include <NTPClient.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "arduino_secrets.h"

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SERIAL_PRINT false
#define DEBUG_PRINT false

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels


#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1, 400000UL);

int DAY_RESET=-1;

char* ssid=SECRET_ssid;
char* pass=SECRET_pass;

const uint16_t port = 8090;
const char * host = "192.168.1.121";

#define BANNER_STRING "Radiation PI v0.89"

/* NTP code vvvvvvvvvvv*/
const long utcOffsetInSeconds = 3600;
char daysOfTheWeek[7][12] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);
/* NTP code ^^^^^^^^^^^*/ 


/* Radiation code vvvvvvvvvvv*/
#define PERIOD_LOG 1                  //Logging period 
const unsigned long int DELAY_DISPLAY_ON_NTP=30;  // seconds
const unsigned long int DELAY_DISPLAY_ON_SENSOR_0=10;  // seconds
const unsigned long int DELAY_DISPLAY_ON_SENSOR_1=10;  // seconds

const unsigned long int PERIOD_COUNT=60;               //Counting period in seconds
const unsigned long int PERIOD_UPDATE_TIME=600;   //seconds 3600      //Period to update NTP clock in seconds
const unsigned long int PERIOD_SEND_SOCKET_0=307;   //seconds 300      //Period to send to server
const unsigned long int PERIOD_SEND_SOCKET_1=101;   //seconds 300      //Period to send to server

const unsigned long int PERIOD_BME_READ=10000; 
const unsigned long int PERIOD_UPDATE_DISPLAY=5; 
const unsigned long int PERIOD_RADIATION_DOUBLECOUNT=10;

//const unsigned long int PERIOD_RECORD_READINGS=20; // seconds 

const int inputPin = 13;
const int ButtonPin = 9;
volatile unsigned long counts = 0;                       // Tube events

unsigned long int lastCountTimeLog;                          // Time measurement for radiation logging
unsigned long int lastCountTimeCount;                        // Time measurement for radiation counting
unsigned long int lastCountSocketSend_0;                       // Time measurement for sending on socket 
unsigned long int lastCountSocketSend_1;                       // Time measurement for sending on socket 
unsigned long int lastCountUpdateNTP;                        // Time measurement for sending on socket 
//unsigned long int lastCountRecord;   

unsigned long int lastRadiationDoubleCount; 

unsigned long int lastBMEmeasure;     
unsigned long int lastDisplayUpdate;         

float cpm = 0;                                           // CPM
int lastCounts = 0;

#define DISPLAY_MAX_ROWS 4
#define DISPLAY_MAX_COLUMNS 22
char DisplayStatic[DISPLAY_MAX_ROWS][DISPLAY_MAX_COLUMNS];
char DisplayLine[DISPLAY_MAX_COLUMNS];
void ICACHE_RAM_ATTR ISR_impulse() { // Captures count of events from Geiger counter board

  if( millis() - lastRadiationDoubleCount > PERIOD_RADIATION_DOUBLECOUNT) {
    counts++;
    if(DEBUG_PRINT && SERIAL_PRINT) {Serial.print(lastRadiationDoubleCount); Serial.println(" ## Count!! ##");}
    lastRadiationDoubleCount = millis();
  } else {
    if(DEBUG_PRINT && SERIAL_PRINT) {Serial.println("## AVOID DOUBLE COUNTING ##");}
    
  }

  //delay(10);
  
}
/* Radiation code ^^^^^^^^^^^*/ 

/* BME sensor vvvvvvvvvvvvvv*/
#define SEALEVELPRESSURE_HPA (1013.25)
Adafruit_BME680 bme; // I2C
float sensTemp,sensPress,sensHum,sensGas;
/* BME sensor ^^^^^^^^^^^^^^*/



#define DATA_RECORD_LENGTH_0 200
#define SENSOR_ID_0 1000
unsigned long int Data_date_0[DATA_RECORD_LENGTH_0];
double Data_values_0[DATA_RECORD_LENGTH_0][1];
unsigned long int data_index_start_0, data_index_stop_0;

#define DATA_RECORD_LENGTH_1 200
#define SENSOR_ID_1 1001
unsigned long int Data_date_1[DATA_RECORD_LENGTH_1];
double Data_values_1[DATA_RECORD_LENGTH_1][4];
unsigned long int data_index_start_1, data_index_stop_1;


void ClearDisplay(){
  for(int i=0;i<DISPLAY_MAX_ROWS;i++){
    for(int j=0;j<DISPLAY_MAX_COLUMNS;j++){
      DisplayStatic[i][j]=0;
    }
  }
}

void PrintDisplayLine(int line, char* text){

  strncpy(&DisplayStatic[line-1][0],text,DISPLAY_MAX_COLUMNS);
  DisplayStatic[line-1][DISPLAY_MAX_COLUMNS-1]='\n';
  display.clearDisplay();
  for(int line_id=0;line_id<DISPLAY_MAX_ROWS;line_id++){
    display.setCursor(0,line_id*8);        
    display.println(DisplayStatic[line_id]);    
  }
  display.display();
}

void setup()
{
  String strDisplayLine;
  
  if(SERIAL_PRINT){
    Serial.begin(115200);
  }

  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    if(SERIAL_PRINT){Serial.println(F("SSD1306 allocation failed"));}
  } else {
    if(SERIAL_PRINT){Serial.println("Connection to display establised..."); }    
  }

  display.clearDisplay();
  display.display();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.cp437(true);
  
  PrintDisplayLine(1,BANNER_STRING);

  
/* Radiation code vvvvvvvvvvv*/
  pinMode(inputPin,INPUT);   //INPUT_PULLUP                         // Set pin for capturing Tube events
  attachInterrupt(digitalPinToInterrupt(inputPin), ISR_impulse, FALLING);     // Define interrupt on falling edge
/* Radiation code ^^^^^^^^^^^*/ 
  pinMode(ButtonPin,INPUT_PULLUP);   //INPUT_PULLUP         
  //attachInterrupt(digitalPinToInterrupt(inputPin), ISR_impulse, FALLING);     // Define interrupt on falling edge
  
  WiFi.begin(ssid, pass);

  strDisplayLine="Connecting.";
  strDisplayLine.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
  PrintDisplayLine(2,&strDisplayLine[0]);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    strDisplayLine+=".";
    if(SERIAL_PRINT){Serial.println("...");}
    strDisplayLine.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
    PrintDisplayLine(2,&strDisplayLine[0]);
  }
  strDisplayLine="Connected:      ";
  strDisplayLine.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
  PrintDisplayLine(2,&strDisplayLine[0]);
  //display.setCursor(0,12);    
  //display.println(strDisplayLine);
  strDisplayLine=WiFi.localIP().toString();
  //display.setCursor(0,24);    
  //display.println(strDisplayLine);
  //display.display();
  strDisplayLine.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
  PrintDisplayLine(3,&strDisplayLine[0]);
 
  if(SERIAL_PRINT){
    Serial.print("WiFi connected with IP: ");
    Serial.println(WiFi.localIP());
  }

/* NTP code vvvvvvvvvvv*/
  timeClient.begin();
  update_NTP_time();
  DAY_RESET=timeClient.getDay();
  
/* NTP code ^^^^^^^^^^*/ 


/* BME sensor vvvvvvvvvvvvvv*/
  if (!bme.begin()) {
    if(SERIAL_PRINT){Serial.println("Could not find a valid BME680 sensor, check wiring!");}
    while (1);
  }
  
  // Set up oversampling and filter initialization
  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320, 150); // 320*C for 150 ms


  
  //display.clearDisplay();
  //display.display();
  //display.setCursor(0,0);         
  //display.println(BANNER_STRING);

  PrintDisplayLine(1,BANNER_STRING);
  
  strDisplayLine="IP: "+WiFi.localIP().toString();    
  //display.println(strDisplayLine);
  strDisplayLine.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
  PrintDisplayLine(2,&strDisplayLine[0]);
  
  strDisplayLine="BME680 initialized";
  //display.println(strDisplayLine);
  strDisplayLine.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
  PrintDisplayLine(3,&strDisplayLine[0]);
  
  strDisplayLine=daysOfTheWeek[timeClient.getDay()];
  strDisplayLine+=", ";
  strDisplayLine+=String(timeClient.getHours());
  strDisplayLine+=":";
  strDisplayLine+=String(timeClient.getMinutes());
  strDisplayLine+=":";
  strDisplayLine+=String(timeClient.getSeconds());
  strDisplayLine+=" UTC";
  
  //display.println(strDisplayLine); 
  //display.display();
  strDisplayLine.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
  PrintDisplayLine(4,&strDisplayLine[0]);
  
  delay(15000);
  if (! bme.performReading()) {
      if(SERIAL_PRINT){Serial.println("Failed to perform reading :(");}
  }  
  else {
    sensTemp=bme.temperature;
    sensPress=bme.pressure/ 100.0;
    sensHum=bme.humidity;
    sensGas=bme.gas_resistance / 1000.0;
  }
  delay(15000);
  if (! bme.performReading()) {
      if(SERIAL_PRINT){Serial.println("Failed to perform reading :(");}
  }  
  else {
    sensTemp=bme.temperature;
    sensPress=bme.pressure/ 100.0;
    sensHum=bme.humidity;
    sensGas=bme.gas_resistance / 1000.0;
  }
  //display.println(BANNER_STRING);
  //strDisplayLine="IP: "+WiFi.localIP().toString(); 
  //display.println(strDisplayLine);
  //display.display();
/* BME sensor ^^^^^^^^^^^^^^*/
  counts = 0;

  data_index_start_0=0;
  data_index_stop_0=0;
  
  data_index_start_1=0;
  data_index_stop_1=0;
  
  ClearDisplay();

  lastBMEmeasure      = millis();
  lastCountSocketSend_0 = millis();  
  lastCountSocketSend_1 = millis();  
  lastCountUpdateNTP  = millis();
  /* Radiation code vvvvvvvvvvv*/
  lastCountTimeLog    = millis();
  lastCountTimeCount  = millis();
/* Radiation code ^^^^^^^^^^^*/
  lastDisplayUpdate   = millis();
  lastRadiationDoubleCount = millis();
  //lastCountRecord     = millis();
}

void update_NTP_time() {
  /* NTP code vvvvvvvvvvv*/
  String str;

 if(SERIAL_PRINT) {
      Serial.print("Entering NTP data update: ");
 }

  
  ClearDisplay();
  str="Updating time: NTP";
  str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
  PrintDisplayLine(1,&DisplayLine[0]);
  
  timeClient.update();
  
  str="NTP time updated";
  str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
  PrintDisplayLine(2,&DisplayLine[0]);
  
  if(SERIAL_PRINT){
    Serial.print(daysOfTheWeek[timeClient.getDay()]);  
    Serial.print(", ");
    Serial.print(timeClient.getHours());
    Serial.print(":");
    Serial.print(timeClient.getMinutes());
    Serial.print(":");
    Serial.println(timeClient.getSeconds());
  }

  str=daysOfTheWeek[timeClient.getDay()];
  str+=", ";
  str+=String(timeClient.getHours());
  str+=":";
  str+=String(timeClient.getMinutes());
  str+=":";
  str+=String(timeClient.getSeconds());
  str+=" UTC";
  str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
  PrintDisplayLine(3,&DisplayLine[0]);


  str="IP: "+WiFi.localIP().toString();   
  str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
  PrintDisplayLine(4,&DisplayLine[0]);


  
  lastDisplayUpdate=millis()+DELAY_DISPLAY_ON_NTP*1000;      
  //lastCountUpdateNTP+=DELAY_DISPLAY_ON_NTP*1000;
  lastCountSocketSend_0=millis()+DELAY_DISPLAY_ON_NTP*1000;
  lastCountSocketSend_1=millis()+DELAY_DISPLAY_ON_NTP*1000;
  //ClearDisplay(); 
  /* NTP code ^^^^^^^^^^*/  
}

void loop()
{
  String str = "";
  char cArray[20];
  int ButtonPressed = digitalRead(ButtonPin);

  
  if(DAY_RESET!=timeClient.getDay()){
     ESP.restart();
  }
 
  
  //delay(100);
  //if(SERIAL_PRINT){
  //  Serial.print(lastDisplayUpdate);
  //  Serial.println("===========+");
  //}
   
  if (ButtonPressed == LOW) {
    if(SERIAL_PRINT){
      Serial.println("----BUTTON------");
      }
  }

  if (millis() > (lastCountTimeCount + PERIOD_COUNT * 1000)) {
    lastCountTimeCount = millis();
    cpm = (counts - lastCounts) / (0.0+PERIOD_COUNT/60.);
    lastCounts = counts;

    // Recording data

    if(data_index_stop_0==DATA_RECORD_LENGTH_0) {
      data_index_stop_0=0;
    } 



    Data_date_0[data_index_stop_0]=timeClient.getEpochTime();
    Data_values_0[data_index_stop_0][0]=cpm;


    data_index_stop_0++;

    if(data_index_stop_0==data_index_start_0){
      data_index_start_0++;
      if(data_index_start_0>DATA_RECORD_LENGTH_0){
        data_index_start_0=0;
      }
    }


    if(SERIAL_PRINT){
      Serial.print("Radiation data record: ");
      Serial.print(data_index_start_0);
      Serial.print(":");
      Serial.print(data_index_stop_0);
      Serial.println();
    }
    
    if(SERIAL_PRINT){
      Serial.println("## CPM ##");
      Serial.print("CPM: "); Serial.println(cpm);
      Serial.println("## CPM ##");
    }
  }

/*
  if (millis() - lastCountRecord > (PERIOD_RECORD_READINGS * 1000)) {
    lastCountRecord = millis();
    if(data_index_stop==DATA_RECORD_LENGTH) {
      data_index_stop=0;
    } 

    if(data_index_stop==data_index_start){
      data_index_start--;
    }

    if(data_index_start<0){
      data_index_start=DATA_RECORD_LENGTH;
    }
    
    Data_date[data_index_stop]=timeClient.getEpochTime();
    Data_values[data_index_stop][0]=cpm;
    Data_values[data_index_stop][1]=sensTemp;
    Data_values[data_index_stop][2]=sensPress;
    Data_values[data_index_stop][3]=sensHum;
    Data_values[data_index_stop][4]=sensGas;

    data_index_stop++;
    
    
    if(SERIAL_PRINT){
      Serial.println("Data record");
    }
  }
   */
  if (millis()  > lastDisplayUpdate + (1000*PERIOD_UPDATE_DISPLAY)) {
    
    if(SERIAL_PRINT){
      Serial.print("UPDATING DISPLAY: ");
      Serial.print("lastDisplayUpdate=");
      Serial.print(lastDisplayUpdate);
      Serial.print(", ");
      Serial.print("millis = ");
      Serial.print(millis());
      Serial.print("  Diff = ");
      Serial.print(millis() - lastDisplayUpdate);
      Serial.print(">");
      Serial.println(1000*PERIOD_UPDATE_DISPLAY);
    }
    
    lastDisplayUpdate=millis();
    
    display.clearDisplay();
    display.display();
    display.setCursor(0,0); 
  
    str="T: ";
    str+=String(sensTemp);
    str+="C ";
    str+="H: ";
    str+=String(sensHum);
    str+="% ";
    
    str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
    PrintDisplayLine(1,&DisplayLine[0]);

    str="P: ";
    str+=String(sensPress);
    str+="hPa";
    str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
    PrintDisplayLine(2,&DisplayLine[0]);
  
    str="Gas: ";
    str+=String(sensGas);
    str+="Kohms ";
    str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
    PrintDisplayLine(3,&DisplayLine[0]);
    
    str="Rad: ";
    str+=String(int(cpm));
    str+=" CPM";
    str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
    PrintDisplayLine(4,&DisplayLine[0]);
  }
  
  /* BME sensor vvvvvvvvvvvvvv*/
  if (millis() >  lastBMEmeasure + PERIOD_BME_READ) {
    lastBMEmeasure = millis();
    if (! bme.performReading()) {
      if(SERIAL_PRINT){Serial.println("Failed to perform reading :(");}
      //return;
    } else {
      sensTemp=bme.temperature;
      sensPress=bme.pressure/ 100.0;
      sensHum=bme.humidity;
      sensGas=bme.gas_resistance / 1000.0;
  
  
      if(data_index_stop_1==DATA_RECORD_LENGTH_1) {
        data_index_stop_1=0;
      } 
  

      Data_date_1[data_index_stop_1]=timeClient.getEpochTime();
      Data_values_1[data_index_stop_1][0]=sensTemp;
      Data_values_1[data_index_stop_1][1]=sensPress;
      Data_values_1[data_index_stop_1][2]=sensHum;
      Data_values_1[data_index_stop_1][3]=sensGas;
  
      data_index_stop_1++;
      
      if(data_index_stop_1==data_index_start_1){
        data_index_start_1++;
        if(data_index_start_1>DATA_RECORD_LENGTH_1){
          data_index_start_1=0;
        }
      }
            
      if(SERIAL_PRINT){
        Serial.print("Ambient data record: ");
        Serial.print(data_index_start_1);
        Serial.print(":");
        Serial.print(data_index_stop_1);
        Serial.println();
      }
      if(DEBUG_PRINT && SERIAL_PRINT){
        Serial.print("Temperature = ");
        Serial.print(sensTemp);
        Serial.println(" *C");
        
        Serial.print("Pressure = ");
        Serial.print(sensPress);
        Serial.println(" hPa");
        
        Serial.print("Humidity = ");
        Serial.print(sensHum);
        Serial.println(" %");
        
        Serial.print("Gas = ");
        Serial.print(sensGas);
        Serial.println(" KOhms");
        
        Serial.print("Approx. Altitude = ");
        Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
        Serial.println(" m");
        
        Serial.println();
      }  
    }  
  }
  /* BME sensor ^^^^^^^^^^^^^^*/
  
  if (millis()  > lastCountUpdateNTP +(PERIOD_UPDATE_TIME * 1000)) {
    lastCountUpdateNTP=millis();
    update_NTP_time();
  }
  /*
  if (millis() - lastCountSocketSend > (PERIOD_SEND_SOCKET * 1000)) {
    lastCountSocketSend=millis();
    if(data_index_stop==data_index_start){
      if(SERIAL_PRINT){Serial.println("No data to send");}
    } else {
    
      WiFiClient client;
     
      if (!client.connect(host, port)) {
        if(SERIAL_PRINT){Serial.println("Connection to host failed");}
        delay(1000);
        return;
      }
     
      if(SERIAL_PRINT){Serial.println("Connected to server successful!");}
      
      
      /*unsigned long epochTime = timeClient.getEpochTime();
      struct tm *ptm = gmtime ((time_t *)&epochTime); 
      int monthDay = ptm->tm_mday;
      int currentMonth = ptm->tm_mon+1;
      int currentYear = ptm->tm_year+1900;
      
      str="";
  
      str+=daysOfTheWeek[timeClient.getDay()];
      str+=", ";
      str+=String(currentYear);
      str+=", ";
      str+=String(currentMonth);
      str+=", "; 
      str+=String(monthDay);
      str+=", ";
      str+=String(timeClient.getHours());
      str+=",";
      str+=String(timeClient.getMinutes());
      str+=",";
      str+=String(timeClient.getSeconds());
      str+=",";
      str+=String(cpm);
      str+=",";
      str+=String(sensTemp);
      str+=",";
      str+=String(sensPress);
      str+=",";
      str+=String(sensHum);
      str+=",";
      str+=String(sensGas);*/

      /*
      unsigned int idx=data_index_start;
      while(idx!=data_index_stop) {
        str=String(Data_date[idx]);
        str+=",";
        str+=String(Data_values[idx][0]);
        str+=",";
        str+=String(Data_values[idx][1]);
        str+=",";
        str+=String(Data_values[idx][2]);
        str+=",";
        str+=String(Data_values[idx][3]);
        str+=",";
        str+=String(Data_values[idx][4]);
        client.println(str);
        idx++;
        if(idx==DATA_RECORD_LENGTH){idx=0;}
      }
      if(SERIAL_PRINT){Serial.println("Disconnecting...");}
      client.stop();

      data_index_start=0;
      data_index_stop=0;      
    }
    
  }*/
  


  if (millis()  > lastCountSocketSend_0 + (PERIOD_SEND_SOCKET_0 * 1000)) {
    lastCountSocketSend_0=millis();

    if(SERIAL_PRINT) {
      Serial.print("Entering Send data (#0) s: ");
      Serial.print(data_index_start_0);
      Serial.print(":");
      Serial.print(data_index_stop_0);
      Serial.println(".");

      Serial.print("lastDisplayUpdate=");
      Serial.print(lastDisplayUpdate);
      Serial.print(", ");
      Serial.print("millis = ");
      Serial.println(millis());
      
      Serial.print("lastCountSocketSend_0=");
      Serial.print(lastCountSocketSend_0);
      Serial.print(", ");
      Serial.print("millis = ");
      Serial.println(millis());
            
      Serial.print("lastCountSocketSend_1=");
      Serial.print(lastCountSocketSend_1);
      Serial.print(", ");
      Serial.print("millis = ");
      Serial.println(millis());
    }

    
    if(data_index_stop_0==data_index_start_0){
      if(SERIAL_PRINT){Serial.println("No data to send for sensor 0");}
    } else {

      ClearDisplay();
      display.clearDisplay();
      str="Uploading sensor 0 to";
      str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
      PrintDisplayLine(1,&DisplayLine[0]);

      str="IP: ";
      str+=String(host);
      str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
      PrintDisplayLine(2,&DisplayLine[0]);

      str="Records: ";
      str+=String(data_index_stop_0-data_index_start_0);
      str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
      PrintDisplayLine(3,&DisplayLine[0]);
      
      WiFiClient client;
     
      if (!client.connect(host, port)) {
        if(SERIAL_PRINT){Serial.println("Connection to host failed");}
        str="Connection failed!!!";      
        str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
        PrintDisplayLine(4,&DisplayLine[0]);
      } else {
     
        if(SERIAL_PRINT){Serial.println("Connected to server successful!");}
 
        unsigned int idx=data_index_start_0;
        while(idx!=data_index_stop_0) {
          str="S,";
          str+=String(SENSOR_ID_0);
          str+=",";
          str+=String(Data_date_0[idx]);
          str+=",";
          str+=String(Data_values_0[idx][0]);
          str+=",E,";
          client.print(str);
          idx++;
          if(idx==DATA_RECORD_LENGTH_0){idx=0;}
        }
        if(SERIAL_PRINT){Serial.println("Disconnecting...");}
        client.stop();
  
        data_index_start_0=0;
        data_index_stop_0=0;      
        str="Transmission complete";      
        str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
        PrintDisplayLine(4,&DisplayLine[0]);
      }
    lastDisplayUpdate=millis()+DELAY_DISPLAY_ON_SENSOR_0*1000;
    lastCountUpdateNTP=millis()+DELAY_DISPLAY_ON_SENSOR_0*1000;
    lastCountSocketSend_1+=DELAY_DISPLAY_ON_SENSOR_0*1000;
    }
     
    if(SERIAL_PRINT){
      Serial.print("lastDisplayUpdate=");
      Serial.print(lastDisplayUpdate);
      Serial.print(", ");
      Serial.print("millis = ");
      Serial.println(millis());
      
      Serial.print("lastCountSocketSend_0=");
      Serial.print(lastCountSocketSend_0);
      Serial.print(", ");
      Serial.print("millis = ");
      Serial.println(millis());
      
      Serial.print("lastCountSocketSend_1=");
      Serial.print(lastCountSocketSend_1);
      Serial.print(", ");
      Serial.print("millis = ");
      Serial.println(millis());
    }
  }


  if (millis()  >  lastCountSocketSend_1 + (PERIOD_SEND_SOCKET_1 * 1000)) {
    
    if(SERIAL_PRINT) {
      Serial.print("Entering Send data (#1) s: ");
      Serial.print(data_index_start_1);
      Serial.print(":");
      Serial.print(data_index_stop_1);
      Serial.println(".");

      Serial.print("lastCountSocketSend_0=");
      Serial.print(lastCountSocketSend_0);

      Serial.print(", ");
      Serial.print("millis = ");
      Serial.println(millis());
      
      Serial.print("lastCountSocketSend_1=");
      Serial.print(lastCountSocketSend_1);

      Serial.print(", ");
      Serial.print("millis = ");
      Serial.println(millis());
    }
  
    lastCountSocketSend_1=millis();
    
    if(data_index_stop_1==data_index_start_1){
      if(SERIAL_PRINT){Serial.println("No data to send for sensor 1");}
    } else {
      if(SERIAL_PRINT){Serial.println("Sending data for sensor 1");}

      ClearDisplay();
      display.clearDisplay();
      str="Uploading sensor 1 to";
      str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
      PrintDisplayLine(1,&DisplayLine[0]);

      str="IP: ";
      str+=String(host);
      str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
      PrintDisplayLine(2,&DisplayLine[0]);

      str="Records: ";
      str+=String(data_index_stop_1-data_index_start_1);
      str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
      PrintDisplayLine(3,&DisplayLine[0]);
          
      WiFiClient client;
     
      if (!client.connect(host, port)) {
        if(SERIAL_PRINT){Serial.println("Connection to host failed");}
        str="Connection failed!!!";      
        str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
        PrintDisplayLine(4,&DisplayLine[0]);
      } else {
     
        if(SERIAL_PRINT){Serial.println("Connected to server successful!");}
 
        unsigned int idx=data_index_start_1;
        while(idx!=data_index_stop_1) {
          str="S,";
          str+=String(SENSOR_ID_1);
          str+=",";
          str+=String(Data_date_1[idx]);
          str+=",";
          str+=String(Data_values_1[idx][0]);
          str+=",";
          str+=String(Data_values_1[idx][1]);
          str+=",";
          str+=String(Data_values_1[idx][2]);
          str+=",";
          str+=String(Data_values_1[idx][3]);
          str+=",E,";
          client.print(str);
          idx++;
          if(idx==DATA_RECORD_LENGTH_1){idx=0;}
        }
        
        if(SERIAL_PRINT){Serial.println("Disconnecting...");}
        client.stop();
  
        data_index_start_1=0;
        data_index_stop_1=0; 
         
        str="Transmission complete";      
        str.toCharArray(&DisplayLine[0],DISPLAY_MAX_COLUMNS);
        PrintDisplayLine(4,&DisplayLine[0]);    
      }
      lastDisplayUpdate=millis()+DELAY_DISPLAY_ON_SENSOR_1*1000;
      lastCountUpdateNTP=millis()+DELAY_DISPLAY_ON_SENSOR_1*1000;
      lastCountSocketSend_0+=DELAY_DISPLAY_ON_SENSOR_1*1000;
      //lastCountSocketSend_1=millis()+DELAY_DISPLAY_ON_SENSOR_1*1000;
      //ClearDisplay(); 
    }
    if(SERIAL_PRINT){
      Serial.print("lastDisplayUpdate=");
      Serial.print(lastDisplayUpdate);
      Serial.print(", ");
      Serial.print("millis = ");
      Serial.println(millis());
      
      Serial.print("lastCountSocketSend_0=");
      Serial.print(lastCountSocketSend_0);
      Serial.print(", ");
      Serial.print("millis = ");
      Serial.println(millis());
      
      Serial.print("lastCountSocketSend_1=");
      Serial.print(lastCountSocketSend_1);
      Serial.print(", ");
      Serial.print("millis = ");
      Serial.println(millis());
    }
  }
 
}
