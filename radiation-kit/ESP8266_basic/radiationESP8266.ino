#define PERIOD_LOG 1                  //Logging period 
const unsigned long int PERIOD_COUNT=60;                //Counting period 

const int inputPin = 13;
volatile unsigned long counts = 0;                       // Tube events

unsigned long lastCountTimeLog;                            // Time measurement
unsigned long lastCountTimeCount;                            // Time measurement

float cpm = 0;                                             // CPM
int lastCounts = 0;


void ICACHE_RAM_ATTR ISR_impulse() { // Captures count of events from Geiger counter board
  counts++;
}

void setup() {
  Serial.begin(115200);
  pinMode(inputPin,INPUT);   // Set pin for capturing Tube events
  attachInterrupt(digitalPinToInterrupt(inputPin), ISR_impulse, FALLING);  // Define interrupt on falling edge

  lastCountTimeLog = millis();
  lastCountTimeCount = lastCountTimeLog;
}

void loop() {
  if (millis() - lastCountTimeLog > (PERIOD_LOG * 1000)) {
    Serial.print("Counts: "); Serial.println(counts); Serial.println("");
    lastCountTimeLog = millis();
  } 
}